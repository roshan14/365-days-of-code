#!/home/roshu/365-days-of-code/myenv/bin/python
from django.core import management

if __name__ == "__main__":
    management.execute_from_command_line()
