from flask import Flask, request
app = Flask(__name__)

@app.route("/")
def hello():
    return '<form action="/echo" method="GET"><input name="text"><input type="submit" value="Echo"></form>'

@app.route("/echo")
def echo():
    return "You said: " + request.args.get('text', '')


if __name__ == "__main__":
    app.run()

# ref
# https://code-maven.com/echo-with-flask-and-python
# http://maximebf.com/blog/2012/10/building-websites-in-python-with-flask/#.WmhVHN99JhE
