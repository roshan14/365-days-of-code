#install python pip
sudo apt-get install python-pip
#install python virtualenv
sudo apt-get install python-virtualenv
# create virtual env
virtualenv  myenv
#activate the virtualenv
. myenv/bin/activate
#install django inside virtualenv
pip install django
#create a new django project
django-admin.py startproject mysite
#enter to the folder of the new django project
cd mysite
#run the django project....
python manage.py runserver .
